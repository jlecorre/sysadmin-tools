#!/bin/bash
#
## @file : set-gnome-desktop-interface.sh
## @author : Joel LE CORRE <j.lecorre+git@sublimigeek.fr>
## @brief : Kill conky, set some keys settings with gsettings and restart conky
## @man : gsettings --help
#

# Variables used in script
value_monospace_font_name="Ubuntu Mono 11"
value_text_scaling_factor="0.95"
conky_pid=0

# Functions used in script
function check_return_code()
{
  if [[ $? -ne 0 ]]
  then
    echo "Ooops ! There is a problem somewhere ..."
    exit 1
  fi
}

# Check if conky is running
conky_pid=$(pgrep -f conky)

# Kill conky if service is running
if [[ -n ${conky_pid} ]]
then
  kill -9 "${conky_pid}"
  check_return_code
fi

# Set gsettings values for Monospace font
gsettings set org.gnome.desktop.interface monospace-font-name "${value_monospace_font_name}"
check_return_code

# Set gsettings values for Text scaling factor
gsettings set org.gnome.desktop.interface text-scaling-factor "${value_text_scaling_factor}"
check_return_code

# Start Conky with new Desktop configuration
/usr/bin/conky -q -d
check_return_code
