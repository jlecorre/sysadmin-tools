#!/bin/bash -eu

#
## @file : execute-borg-backup.sh
## @author : Joel LE CORRE <git@sublimigeek.fr>
## @brief : script used to generate backups based on the Borg tool (https://www.borgbackup.org/)
## @links : https://gitlab.com/jlecorre/sysadmin-tools
#

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Before to execute this script, you need to follow these requirements:
# - Configure a NFS share to host your backup archives
# - Initialize a Borg repository (https://borgbackup.readthedocs.io/en/stable/usage/init.html)
#   with the 'repokey-blake2' encryption method (encrypted and authenticated method)
# - Customize the variables below
# - Don't forget to export your repository key
# - Then schedule a cron or a systemD timer to execute it and enjoy your encrypted backups!
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Script configuration
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Mail configuration
EMAIL_SRC=""
EMAIL_DST=""
EMAIL_OBJECT_PREFIX=""

# Mount point configuration
MOUNT_POINT_SRC=""
MOUNT_POINT_DST=""

# Borg configuration
BORG_EXECUTABLE_PATH="/usr/bin/borg"
BORG_MAIN_REPOSITORY_PATH=""
BORG_BACKUP_NAME_PREFIX=""
BORG_BACKUP_NAME_SUFFIX=$(date '+%Y-%m-%d-%H-%M')
BORG_BACKUP_LOG_FOLDER="/var/log/borg"
BORG_BACKUP_LOG_FILE="${BORG_BACKUP_LOG_FOLDER}/${BORG_BACKUP_NAME_PREFIX}-${BORG_BACKUP_NAME_SUFFIX}.log"
export BORG_RELOCATED_REPO_ACCESS_IS_OK=no
# Ensures that backups are not created on random drives that “just happen” to contain a Borg repository
export BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK=no
# Passphrase used as authentication and encryption key of the Borg repository
export BORG_PASSPHRASE=''
BORG_OPTS="--stats --one-file-system --compression lz4 --checkpoint-interval 86400"
# List of folders to backup separated by a space
BORG_FOLDERS_LIST=""

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Functions definition
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

check_status_config()
{
  if [ -z "${EMAIL_SRC}" ] || [ -z "${EMAIL_DST}" ] || [ -z "${EMAIL_OBJECT_PREFIX}" ] \
    || [ -z "${MOUNT_POINT_SRC}" ] || [ -z "${MOUNT_POINT_DST}" ] \
    || [ -z "${BORG_MAIN_REPOSITORY_PATH}" ] || [ -z "${BORG_BACKUP_NAME_PREFIX}" ] \
    || [ -z "${BORG_PASSPHRASE}" ] || [ -z "${BORG_FOLDERS_LIST}" ]; then
    print_message "error" "One or more required variables need to be declared!"
    cat "${BORG_BACKUP_LOG_FILE}"
    exit 1
  fi
}

check_status_code()
{
  local _status=$1
  local _error_message=$2

  if [ "${_status}" -ne 0 ]; then
    print_message "error" "${_error_message}"

    if [ "$(stat -f -L -c %T ${BORG_MAIN_REPOSITORY_PATH})" == "nfs" ]; then
      umount_nfs_share
    fi

    send_mail "${EMAIL_OBJECT_PREFIX} - Backup report for ${BORG_BACKUP_NAME_SUFFIX} => ERROR" "${BORG_BACKUP_LOG_FILE}"
    exit 1
  fi
}

is_installed()
{
  if ! command -v "${BORG_EXECUTABLE_PATH}" >/dev/null 2>&1
  then
    check_status_code "1" "The <borg> package is not installed! (https://borgbackup.readthedocs.io/en/stable/installation.html)"
  else
    print_message "info" "Borg binary is located here: ${BORG_EXECUTABLE_PATH}"
  fi
}

is_folder_exists()
{
  local _folder_name=$1

  if [ ! -d "${_folder_name}" ]; then
    mkdir -p "${_folder_name}"
    check_status_code $? "Could not properly create required folder: ${_folder_name}"
    print_message "info" "<${_folder_name}> folder has been created."
  fi
}

send_mail()
{
  local _mail_title="$1"
  local _mail_body="$2"

  mail -E -r "${EMAIL_SRC}" -s "${_mail_title}" "${EMAIL_DST}" < "${_mail_body}"
}

print_message()
{
  local _message_kind=$1
  local _message=$2

  printf "[$(date '+%Y/%m/%d:%H:%M:%S')] - ${_message_kind^^} - %s\\n" "${_message^}" >> "${BORG_BACKUP_LOG_FILE}"
}

mount_nfs_share()
{
  if ! /usr/bin/mountpoint -q "${MOUNT_POINT_DST}" ; then
    /usr/sbin/mount.nfs4 "${MOUNT_POINT_SRC}" "${MOUNT_POINT_DST}"
    check_status_code $? "Could not properly mount the NFS share: ${MOUNT_POINT_DST}"
    print_message "info" "NFS share is mounted on: ${MOUNT_POINT_DST}"
  fi
}

umount_nfs_share()
{
  if /usr/bin/mountpoint -q "${MOUNT_POINT_DST}" ; then
    /usr/sbin/umount.nfs4 "${MOUNT_POINT_DST}"
    check_status_code $? "Could not properly unmount the NFS share: ${MOUNT_POINT_DST}"
    print_message "info" "NFS share ${MOUNT_POINT_DST} is unmounted."
  fi
}

execute_backup()
{
  print_message "info" "Borg version used to create new backup"
  ${BORG_EXECUTABLE_PATH} --version >> "${BORG_BACKUP_LOG_FILE}"
  check_status_code $? "Could not verify the Borg version number!"

  print_message "info" "Starting repository check process..."
  ${BORG_EXECUTABLE_PATH} check --info --repository-only "${BORG_MAIN_REPOSITORY_PATH}" >> "${BORG_BACKUP_LOG_FILE}" 2>&1
  check_status_code $? "Repository check reports a problem!"
  print_message "info" "Check repository execution: DONE"

  print_message "info" "Starting backup process..."
  # shellcheck disable=SC2086,SC2086
  ${BORG_EXECUTABLE_PATH} create ${BORG_OPTS} "${BORG_MAIN_REPOSITORY_PATH}::${BORG_BACKUP_NAME_PREFIX}-${BORG_BACKUP_NAME_SUFFIX}" ${BORG_FOLDERS_LIST} >> "${BORG_BACKUP_LOG_FILE}" 2>&1
  check_status_code $? "Could not generate the daily backup!"
  print_message "info" "Backup execution: DONE"

  print_message "info" "Starting daily archive check process..."
  ${BORG_EXECUTABLE_PATH} check --info --archives-only "${BORG_MAIN_REPOSITORY_PATH}::${BORG_BACKUP_NAME_PREFIX}-${BORG_BACKUP_NAME_SUFFIX}" >> "${BORG_BACKUP_LOG_FILE}" 2>&1
  check_status_code $? "Archive check reports a problem!"
  print_message "info" "Check archive execution: DONE"
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Main program
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

is_installed
check_status_config

is_folder_exists "${BORG_BACKUP_LOG_FOLDER}"
is_folder_exists "${MOUNT_POINT_DST}"

mount_nfs_share
sleep 5
execute_backup
sleep 5
umount_nfs_share

send_mail "${EMAIL_OBJECT_PREFIX} - Backup report for ${BORG_BACKUP_NAME_SUFFIX} => SUCCESS" "${BORG_BACKUP_LOG_FILE}"

