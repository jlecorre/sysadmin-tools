# Global GIT configuration files

## Description
This files are useful to correctly customize your GIT installation.  
If your copy or create symbolic link of this file in your ${HOME}, this configuration will be enable on **all yours code repository**.

## Included files
* `.gitconfig` = customized the behavior of GIT
* `.gitignore` = customized the behavior of excluded files

## Instructions
To enable this configuration files, clone the repository :
```
git clone ${this_repository}
cd ${this_repository}
```
And create symbolic link with this command lines :
```
echo "ln -s $(pwd)/.gitconfig ${HOME}/.gitconfig"
echo "ln -s $(pwd)/.gitconfig ${HOME}/.gitignore"
```
And check yours customizations are enables :
```
git config --list
```
Enjoy =)
