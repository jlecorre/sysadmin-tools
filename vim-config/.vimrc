"""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Configuration personnelle de l'éditeur de texte VIM "
" Mainteneur : Joel LE CORRE <git@sublimigeek.fr>     "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""""""
" Configuration de Vundle (plugins manager for vim) "
"""""""""""""""""""""""""""""""""""""""""""""""""""""

"
"" Installation du plugin Vundle :
"" mkdir -p $HOME/.vim/bundle ; cd $HOME/.vim/bundle
"" git clone https://github.com/VundleVim/Vundle.vim.git
"" Création de répertoires utiles
"" mkdir -p $HOME/.vim/{sessions,undodir}
"

" Be iMproved vim, required
set nocompatible
" Désactivation de la détection du type des fichiers
filetype off
" Définition du chemin d'exécution pour initialiser Vundle
set rtp+=~/.vim/bundle/Vundle.vim
" Appel du plugin manager Vundle
call vundle#begin()
" Let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
" Installation du plugin Vim-Airline
Plugin 'vim-airline/vim-airline'
" Installation des thèmes du plugin Vim-Airline
Plugin 'vim-airline/vim-airline-themes'
" Installation du plugin Python-mode
Plugin 'python-mode/python-mode', { 'branch': 'develop' }
" Installation du plugin Dockerfile.vim
Plugin 'docker/docker', {'rtp': '/contrib/syntax/vim/'}
" Installation du thème PaperColor
Plugin 'NLKNguyen/papercolor-theme'
" Installation du plugin NERD Tree
Plugin 'scrooloose/nerdtree'
" Installation du plugin NERD Comment
Plugin 'scrooloose/nerdcommenter'
" Installation du plugin RuboCop
Plugin 'ngmy/vim-rubocop'
" Installation du plugin File-line
Plugin 'bogado/file-line'
" Installation du plugin jinja.vim
Plugin 'lepture/vim-jinja'
" Installation du plugin vim-markdown
Plugin 'godlygeek/tabular'
" Installation de plugins dédié à Terraform
Plugin 'hashivim/vim-terraform'
Plugin 'juliosueiras/vim-terraform-completion'
" Installation du plugin vim-go
" Documentation : https://github.com/fatih/vim-go/blob/master/doc/vim-go.txt
Plugin 'fatih/vim-go'

" Gestion de la coloration syntaxique
" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
" /!\ Pour bénéficier de la fonctionnalité SHELLCHECK : [optionnel]
" /!\ Il faut installer le paquet suivant : sudo apt install shellcheck
" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Plugin 'vim-syntastic/syntastic'
" Tous les plugins doivent être ajoutés avant cette ligne
call vundle#end()            " required
filetype plugin indent on    " required

" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
" See :h vundle for more details

"""""""""""""""""""""""""""""""""""""""""""""""""
" Configuration du thème PaperColor             "
" https://github.com/NLKNguyen/papercolor-theme "
"""""""""""""""""""""""""""""""""""""""""""""""""
" Activation du thème PaperColor
colorscheme PaperColor
" Activation de la table des 256 couleurs
set t_Co=256
" Définie la couleur de la syntaxe et de l'arrière plan
set background=dark
" Activation de la barre d'états
set laststatus=2

""""""""""""""""""""""""""""""""""""""""""""""
" Configuration du plugin vim-airline        "
" https://github.com/vim-airline/vim-airline "
""""""""""""""""""""""""""""""""""""""""""""""
" Activation du plugin vim-airline avec PaperColor
" Themes ++ : raven, ubarid, kolor, lucius, luna, term, understated
let g:airline_theme='understated'
" Activation|Désactivation des fonts Powerline (NB: les fonts Powerline sont mal interpretées dans un screen)
let g:airline_powerline_fonts = 0
" Activation de la gestion intelligente des onglets
let g:airline#extensions#tabline#enabled = 1
" Configuration des séparateurs de la tabline
" let g:airline#extensions#tabline#left_sep = ' '
" let g:airline#extensions#tabline#left_alt_sep = '|'
" Gestion du formatage de l'affichage des noms de fichiers dans les onglets
" Valeurs possibles : default | jsformatter | unique_tail | unique_tail_improved
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'
" Activation de la détection du changement d'état des buffers
let g:airline_detect_modified=1
let g:airline_detect_paste=1
let g:airline_detect_crypt=1
let g:airline_detect_iminsert=0
let g:airline_inactive_collapse=0
let g:bufferline_echo = 0
" Si les Powerline fonts sont desactivées, on spécifie les symboles utilisés pour la barre de statuts
let g:airline_left_sep='>'
let g:airline_left_sep='>'
let g:airline_right_sep='<'
" Activation du plugin syntastic dans vim-airline
let g:airline#extensions#syntastic#enabled = 1

""""""""""""""""""""""""""""""""""""""""""""""
" Configuration du plugin python-mode        "
" https://github.com/python-mode/python-mode "
""""""""""""""""""""""""""""""""""""""""""""""
" Activer la vérification syntaxique pour Python3
let g:pymode_python = 'python3'

""""""""""""""""""""""""""""""""""""""""""
" Configuration du plugin NERD Tree      "
" https://github.com/scrooloose/nerdtree "
""""""""""""""""""""""""""""""""""""""""""
" Activation de NERD Tree si VIM est exécuté sans paramètre
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
" Mapping d'un raccourvi clavier pour l'ouverture du plugin
map <C-x> :NERDTreeToggle<CR>

"""""""""""""""""""""""""""""""""""""""""""""""
" Configuration du plugin NERD Commenter      "
" https://github.com/scrooloose/nerdcommenter "
"""""""""""""""""""""""""""""""""""""""""""""""

" Brief help
" <leader>cc       - commenter les lignes en mode visuel
" <leader>cy       - copie les lignes sélectionnées avant de les commenter
" <leader>cu       - décommenter les lignes en mode visuel
" <leader>c<space> - Inverse l'état commenté/non-commenté de la sélection

" Ajout d'espaces après les délimiteurs de commentaires par défaut
let g:NERDSpaceDelims = 1
" Utiliser une syntaxe compacte pour les commentaires multi-lignes
let g:NERDCompactSexyComs = 1
" Aligner les délimiteurs de commentaires par ligne à gauche au lieu de suivre l'indentation du code
let g:NERDDefaultAlign = 'left'
" Définir une langue pour utiliser ses délimiteurs alternatifs par défaut
let g:NERDAltDelims_java = 1
" Permet d'ajouter ses propres formats personnalisés ou de remplacer les valeurs par défaut
let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }
" Permet de commenter et d'inverser les lignes vides (utile pour commenter une région)
let g:NERDCommentEmptyLines = 1
" Activer l'ajustement de l'espacement des blancs de fin de ligne lorsque vous décommentez
let g:NERDTrimTrailingWhitespace = 1
" Activer NERDCommenterToggle pour vérifier que toutes les lignes sélectionnées sont commentées ou non
let g:NERDToggleCheckAllLines = 1

"""""""""""""""""""""""""""""""""""""""""""
" Configuration du plugin Syntastic       "
" https://github.com/scrooloose/syntastic "
"""""""""""""""""""""""""""""""""""""""""""
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

""""""""""""""""""""""""""""""""""""""""""""""
" Configuration du plugin Vim Markdown       "
" https://github.com/plasticboy/vim-markdown "
""""""""""""""""""""""""""""""""""""""""""""""

" Désactivation du pliage (aka folding configuration)
"let g:vim_markdown_folding_disabled = 1
" Mettre en évidence le YAML
let g:vim_markdown_frontmatter = 1

"""""""""""""""""""""""""""""""""""""""""""""
" Configuration du plugin Vim Vim Terraform "
" https://github.com/hashivim/vim-terraform "
"""""""""""""""""""""""""""""""""""""""""""""
" Autoriser le plugin à modifier le .vimrc pour l'indentation
let g:terraform_align=0
" Masquer les blocs de code Terraform
let g:terraform_fold_sections=1
" Mapper la touche <espace> pour afficher/masquer les blocs de code
let g:terraform_remap_spacebar=1
" Personnaliser le pattern de commentaires
let g:terraform_commentstring='#%s'

"""""""""""""""""""""""""""""""""""""""""""""""""""
" Configuration personelle du comportement de VIM "
"""""""""""""""""""""""""""""""""""""""""""""""""""
" Désactivation du Background Color Erase
set t_ut=
" Définie la taille d'une tabulation à 2 espaces
set tabstop=2
" Longueur des décalages VIM = 2
set shiftwidth=2
" Nombre d'espaces composant une tabulation
set softtabstop=2
" L'insertion d'une tabulation génère l'insertion d'espaces
set expandtab
" Tabulations automatiques
set smarttab
" Activation de la coloration syntaxique
syntax on
" Prise en charge de l'encodage UTF-8
set encoding=utf-8
" Indentations automatiques
set autoindent
" Indentations automatiques ET intelligentes
set smartindent
" Active le copié/collé prenant en compte le formatage des données
set paste
" Niveau de l'historique
set history=1000
" Gestion de l'historique des annulations entre les sessions (a.k.a persistent undo)
set undofile
" Chemin de stockage des fichiers d'historique des modifications
" /!\ Le répertoire doit être créé manuellement : mkdir ~/.vim/undodir
set undodir=~/.vim/undodir
" Permet la mise à jour du fichier lu par VIM si celui-ci est modifié depuis l'extérieur
" (seulement lors de l'exécution d'une commande externe, exemple :e! ou :!ls)
set autoread
" Affiche toujours la position courante du curseur
set ruler
" Ignore la case lors d'une recherche
set ignorecase
" Activation de la recherche intelligente
set smartcase
" Surligne les résultats des recherches
set hlsearch
" Déplace le curseur au fur et à mesure de la saisie lors d'une recherche
set incsearch
" Permet l'utilisation des expressions regulières
set magic
" Surligne les couples d'accolades/parenthèses
set showmatch
" Affichage des commandes utilisées en mode normal
set showcmd
" Activation du wrapping (retour à la ligne automatique)
set wrap
" Nombre de caractères depuis le bord extérieur
set wrapmargin=8
" Améliore la complétion des commandes
set wildmenu
" Utiliser le format de fichiers standard Unix
set ffs=unix,dos,mac
" Ne réactualise pas les buffers lors de l'exécution d'une macro
set lazyredraw
" Permet de déplacer le curseur sur les longues lignes
map j gj
map k gk
" Modification du mapping de la touche <leader> (par défaut <\>)
let mapleader=","
" Raccourcis claviers permettant la gestion des onglets/buffers
map <Leader>tn :tabnew<cr>
map <Leader>to :tabonly<cr>
map <Leader>tc :tabclose<cr>
map <Leader>tm :tabmove<cr>
" Sauvegarder un fichier nécessitant des privilèges élevés
noremap <Leader>W :w !sudo tee % > /dev/null
" Sauvegarder un fichier nécessitant des privilèges élevés
" (2nd raccourci clavier)(source: jovicailic.org)
cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!
" Affiche/masque les numéros de lignes à la demande
map <Leader>nb :set invnumber<cr>
" Permet de forcer le rafraichissement du fichier courant via un raccouci clavier
map <Leader>fr :edit!<cr>
" Paramétrage de la largeur de la colonne des numéros de lignes
set numberwidth=1
" Activation du pliage des classes/méthodes
set foldmethod=indent
set foldlevel=99
" Activation du pliage avec la touche espace
nnoremap <space> za
" Remap key binding used to enable visual block mode
:nnoremap <Leader>v <c-v>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Définition de la fonction MatchAndRemoveSpaceCharacter "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
func! MatchAndRemoveSpaceCharacter()
  exe "normal mz"
  :%s/\s\+$//ge
  exe "normal `z"
endfunc
" Création d'un raccourci clavier appellant la fonction MatchAndRemoveSpaceCharacter
map <Leader>rs :call MatchAndRemoveSpaceCharacter()<CR>
" Exécute automatiquement la fonction MatchAndRemoveSpaceCharacter pour les scripts Python
autocmd BufWrite *.py :call MatchAndRemoveSpaceCharacter()

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Revenir à la position précédente du curseur si cela est possible "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
autocmd BufReadPost *
      \ if line("'\"") > 0 && line("'\"") <= line("$") |
      \   exe "normal g`\"" |
      \ endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Configuration du comportement de la fonctionnalité keywordprg "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Utilisation du programme ansible-doc pour les fichiers YAML
autocmd BufNewFile,BufRead,FileType *.yml set keywordprg=ansible-doc
" Utilisation du programme pydoc pour les fichiers Python
autocmd BufNewFile,BufRead *.py set keywordprg=pydoc

""""""""""""""""""""""""""""""""""""""""""
" Configurations personelles désactivées "
""""""""""""""""""""""""""""""""""""""""""
" Permet de travailler avec un fond d'écran sombre
"set background=dark (désactivé pour prise en compte des thèmes)
" Désactive les anciens raccourcis claviers de VI
"set nocompatible (désactivé ici car spécifié dans configuration de Vundle)
" Désactivation du système de sauvegarde
"set nobackup
"set nowb
"set noswapfile
