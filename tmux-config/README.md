# How to finalize Tmux configuration

## Install required plugins

To properly display the status bar, several plugins are required.  
Follow the available instructions of this documentation to be able to finalize the Tmux customization.

### kube-tmux plugin

``` bash
git clone https://github.com/jonmosco/kube-tmux.git ~/.tmux/kube-tmux
```

### tmux-load-average

``` bash
git clone https://github.com/dastergon/tmux-load-avg.git ~/.tmux/tmux-load-avg
```
