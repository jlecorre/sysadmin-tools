Ansible
=======

Ansible is a radically simple IT automation system.  It handles configuration-management, application deployment, cloud provisioning, ad-hoc
task-execution, and multinode orchestration - including trivializing things like zero downtime rolling updates with load balancers.

Read the documentation and more at https://ansible.com/

Ansible configuration file
==========================

Configurations files and/or variables are read in this order for
Ansible version newer thant 1.5 :
* ANSIBLE_CONFIG (an environment variable)
* ansible.cfg (in the current directory)
* .ansible.cfg (in the home directory)
* /etc/ansible/ansible.cfg

How to use this config file ?
=============================

It's easy :
```
ln -s /path/to/this/repository/ansible-config/.ansible.cfg  ${HOME}
```
And now just played your playbooks :)
