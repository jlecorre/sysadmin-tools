#!/bin/bash -eu

#
## @file : force-monitors-disconnection.sh
## @author : Joel LE CORRE <git@sublimigeek.fr>
## @brief : script used to list active monitors and force disconnect them by using Xrandr CLI
## @links : https://man.archlinux.org/man/xrandr.1
#

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Script configuration
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

AWK_BINARY_PATH="/usr/bin/awk"
XRANDR_BINARY_PATH="/usr/bin/xrandr"
declare -a ACTIVE_MONITORS_LIST

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Functions definition
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

list_active_monitors_and_clean_output()
{
  # shellcheck disable=SC2016
  XRANDR_OUTPUT=$($AWK_BINARY_PATH '/\<connected\>/ {print $1}' < <($XRANDR_BINARY_PATH) | grep -v eDP)
  for SCREEN in ${XRANDR_OUTPUT};
  do
    ACTIVE_MONITORS_LIST+=("--output ${SCREEN} --off ")
  done

  eval "${XRANDR_BINARY_PATH} --output eDP --auto ${ACTIVE_MONITORS_LIST[*]}"
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Main program
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

list_active_monitors_and_clean_output
