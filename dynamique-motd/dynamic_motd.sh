#!/bin/bash
# shellcheck disable=SC2086,SC2046,SC2004,SC2068

#
## @file : dynamic_motd.sh
## @author : Joel LE CORRE <gitlab@sublimigeek.fr>
## @brief : script generating a dynamic MOTD (a.k.a Meesage Of The Day)
## @links : https://gitlab.com/jlecorre/sysadmin-tools
#

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# color management for user display
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
export BBlack='\e[1;30m'  # Bold and Black
export BRed='\e[1;31m'    # Bold and Red
export BGreen='\e[1;32m'  # Bold and Green
export BYellow='\e[1;33m' # Bold and Yellow
export BBlue='\e[1;34m'   # Bold and Blue
export BPurple='\e[1;35m' # Bold and Purple
export BCyan='\e[1;36m'   # Bold and Cyan
export BWhite='\e[1;37m'  # Bold and White
export Color_Off='\e[0m'  # Text Reset

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# processing to generate MOTD data
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# function for listing information about active network interfaces
function check_interface()
{
  # list all active interfaces on the system
	interface_number_addr_info=$(ip -j addr show "${interface}" | jq -r '.[] | .addr_info | length')
  interface_contains_inet6=$(ip -j addr show "${interface}" | jq -r '.[] | .addr_info[].family | contains("inet6")')
  # if the interface has an IPv6 address, it will displayed
  if [[ ${interface_contains_inet6} ]] && [ "${interface_number_addr_info}" -gt 1 ]
  then
    printf "${BBlack}Network Interface IPv4..........: ${Color_Off}${BPurple}${interface} - %s${Color_Off}\n" "$(ip -4 addr show "${interface}" | sed -nr 's/.*inet ([^ ]+).*/\1/p')"
    printf "${BBlack}Network Interface IPv6..........: ${Color_Off}${BPurple}${interface} - %s${Color_Off}\n" "$(ip -6 addr show "${interface}" | sed -nr 's/.*inet6 ([^ ]+).*/\1/p' | head -n1)"
  # otherwise, only informations about the IPv6 address are processed
  elif [[ ${interface_contains_inet6} ]] && [ "${interface_number_addr_info}" = 1 ]
  then
    printf "${BBlack}Network Interface IPv6..........: ${Color_Off}${BPurple}${interface} - %s${Color_Off}\n" "$(ip -6 addr show "${interface}" | sed -nr 's/.*inet6 ([^ ]+).*/\1/p' | head -n1)"
  # otherwise, only informations about the IPv4 address are processed
  elif [[ ${interface_contains_inet6} == false ]] && [ "${interface_number_addr_info}" = 1 ]
  then
    printf "${BBlack}Network Interface IPv4..........: ${Color_Off}${BPurple}${interface} - %s${Color_Off}\n" "$(ip -4 addr show "${interface}" | sed -nr 's/.*inet ([^ ]+).*/\1/p')"
  else
    printf "${BBlack}Network Interface...............: ${Color_Off}${BPurple}%s has no IP address !${Color_Off}\n" "${interface}"
  fi
}

# function to calculate uptime of the system
function get_uptime()
{
  # get the server's uptime and convert it into hours
  server_uptime=$(</proc/uptime)
  server_uptime=${server_uptime%%.*}
  time_seconds=$(( server_uptime%60 ))
  time_minutes=$(( server_uptime/60%60 ))
  time_hours=$(( server_uptime/60/60%24 ))
  time_days=$(( server_uptime/60/60/24 ))
  # return result
  uptime="$time_days days, $time_hours hours, $time_minutes minutes, $time_seconds secondes"
  printf "%s" "${uptime}"
}

# retrieving system memory informations
# shellcheck disable=SC2207
get_memory_informations=($(</proc/meminfo))

# function for retrieving system memory status information
function get_memory_status()
{
  memory_status=""
  for value in "${!get_memory_informations[@]}"
  do
    # here, we filtered only interesting data
    if [ "${get_memory_informations[$value]}" == "MemTotal:" ] || [ "${get_memory_informations[$value]}" == "MemFree:" ] || [ "${get_memory_informations[$value]}" == "Cached:" ]
    then
      # converting KB into MB
      memory_size_mb="$(( ${get_memory_informations[$value+1]} / 1024 )) MB"
      memory_status="$memory_status ${get_memory_informations[$value]} ${memory_size_mb}"
    fi
  done
  printf "%s" "${memory_status[@]}"
}

# function for retrieving informations about the status of the swap memory of the system
function get_swap_status()
{
  swap_status=""
  for value in "${!get_memory_informations[@]}"
  do
    # here, we filtered only interesting data
    if [ "${get_memory_informations[$value]}" == "SwapTotal:" ] || [ "${get_memory_informations[$value]}" == "SwapFree:" ] || [ "${get_memory_informations[$value]}" == "SwapCached:" ]
    then
      # converting KB into MB
      swap_size_mb="$(( ${get_memory_informations[$value+1]} / 1024 )) MB"
      swap_status="$swap_status ${get_memory_informations[$value]} ${swap_size_mb}"
    fi
  done
  printf "%s" "${swap_status[@]}"
}

# get hostname of the system
upper_hostname=$(hostname | tr "[:lower:]" "[:upper:]")

# get informations about system kernel
kernel_informations=$(uname -iomrs)

# get distribution release informations
system_informations=$(lsb_release -d | awk -F ':' '{ print $2 }' | sed -e 's/^[[:space:]]*//')

# generate list of connected hard drives
devices_list=$(sudo smartctl --scan | awk -F ' ' '{ print $1 }')

# get temperature of hard drives
function check_disk_temperature()
{
  for device in ${devices_list}
  do
    hdd_temperature=$(sudo hddtemp -w --unit C "${device}" | awk -F': ' '{ print $2 " : " $3 }')
    printf "${BBlack}Disk Temperature of ${device}....: ${Color_Off}${BPurple}%s\n${Color_Off}" "${hdd_temperature}"
  done
}

# get informations about last connection on the system
last_login=$(last -n1 -i | head -n1)

# get informations about CPU
cpu_information=$(grep -m 1 "model name" /proc/cpuinfo | awk -F': ' '{print $2}')

# list of all active SSH sessions
ssh_login=$(who -q | tail -n1 | cut -d'=' -f2)" active SSH session(s)"

# get the percentage of rootfs disk utilization
rootfs_usage=$(df -h / | awk '/\// {print $(NF-1)}')" of "$(df -h / | awk '/\// {print $2}')

# get system load average
system_load_avg=$(< /proc/loadavg awk '{print $1" "$2" "$3}')

# function displaying S.M.A.R.T status of hard drives
function check_smartctl_state()
{
  # if script isn't executed by root, display message to user
  if [[ $(id -u) -ne 0 ]]
  then
    printf "To scan the S.M.A.R.T state of device <%s>, you need to be root ...\n" "${device}"
  else
    # get S.M.A.R.T status of hard drives
    get_disk_state=$(sudo smartctl -q noserial --json -H "${device}" | jq -r '.smart_status.passed')
    # display results to user
    if [[ $get_disk_state ]]
    then
      get_disk_state="PASSED"
    else
      get_disk_state="ERROR"
    fi
    printf "${BBlack}S.M.A.R.T state of ${device}.....:${Color_Off}${BPurple} %s\n${Color_Off}" "${get_disk_state}"
  fi
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# generation and display of the MOTD
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# cleaning the terminal
clear

# displaying header
cat << "EOF"

                                          __________________________________
                                         /    Si supervision tu n'as pas,   \
                                         \    nuit blanche tu feras ...     /
                                          ----------------------------------
                                            \
                                             \
                                                 .--.
                                                |o_o |
                                                |:_/ |
                                               //   \ \
                                              (|     | )
                                             /'\_   _/`\
                                             \___)=(___/

EOF

# displaying and formatting of information gathered by the script
echo -e "${BWhite}#-------------------------------------~~ System Informations ~~---------------------------------------#${Color_Off}"
printf "${BBlack}Hostname........................:${Color_Off}${BPurple} %s\n${Color_Off}" "${upper_hostname}"
printf "${BBlack}Kernel informations.............:${Color_Off}${BPurple} %s\n${Color_Off}" "${kernel_informations}"
printf "${BBlack}Operation System informations...:${Color_Off}${BPurple} %s\n${Color_Off}" "${system_informations}"
printf "${BBlack}Informations about CPU..........:${Color_Off}${BPurple} %s\n${Color_Off}" "${cpu_information}"
echo -e "${BWhite}#-------------------------------------~~ Operating system Informations ~~-----------------------------#${Color_Off}"
printf "${BBlack}Uptime..........................:${Color_Off}${BPurple} %s\n${Color_Off}" "$(get_uptime)"
printf "${BBlack}Last login......................:${Color_Off}${BPurple} %s\n${Color_Off}" "${last_login//    /}" # trim useless whitespaces
printf "${BBlack}SSH Logins......................:${Color_Off}${BPurple} %s\n${Color_Off}" "${ssh_login}"
printf "${BBlack}Load Average....................:${Color_Off}${BPurple} %s\n${Color_Off}" "${system_load_avg}"
printf "${BBlack}Memory (MB).....................:${Color_Off}${BPurple}%s\n${Color_Off}" "$(get_memory_status)"
printf "${BBlack}Swap (MB).......................:${Color_Off}${BPurple}%s\n${Color_Off}" "$(get_swap_status)"
echo -e "${BWhite}#-------------------------------------~~ Network Informations ~~--------------------------------------#${Color_Off}"
# declaration of an array containing different network interface informations
for interface in $(ip -j link show | jq -r '.[].ifname')
do
    # for each interface the check_interface function is executed
    check_interface
done
echo -e "${BWhite}#--------------------------------------~~ Disks Informations ~~---------------------------------------#${Color_Off}"
printf "${BBlack}Disk Usage (rootfs).............:${Color_Off}${BPurple} %s\n${Color_Off}" "${rootfs_usage}"
check_disk_temperature
# displaying informations collected by the check_smartctl_state function
for device in ${devices_list}
do
  # call up the function collecting information S.M.A.R.T
  check_smartctl_state "${device}"
done
echo -e "${BWhite}#-----------------------------------------------------------------------------------------------------#${Color_Off}\n"
