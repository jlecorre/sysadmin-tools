#!/bin/bash
#
## @file : init-ssh-agent.sh
## @author : Joel LE CORRE <gitlab@sublimigeek.fr>
## @brief :
##    * Démarre un agent SSH
##    * Charge une clef SSH
##    * Exporte la variable $SSH_AUTH_SOCK
## @liens :
## - Wiki : voir le README.md du dépôt de code :
#
set -x
# SOURCES : 
# https://gist.github.com/duijf/baeeec8197cdf81448370cf33dc0e708

# Définition des variables utiles à l'exécution du script
readonly SSH_AVAILABLE_KEYS="$HOME/.ssh/*"
readonly SSH_AGENT_PID=$(pidof "ssh-agent")
readonly SSH_AGENT_EXE="/usr/bin/ssh-agent"
readonly SSH_ADD_EXE="/usr/bin/ssh-add"
readonly PEM_FILE_TYPE="PEM RSA private key"
readonly FILE_EXE="/usr/bin/file"

function f_start_agent {
	# On vérifie si l'agent SSH est démarré ou non
	if [[ ! -z $SSH_AGENT_PID ]] # TODO - inverser la condition une fois les tests OK
	then
    printf "Démarrage de l'agent-ssh ..."
		eval "$($SSH_AGENT_EXE)"
		f_check_return_code
		f_load_key
    f_check_return_code
	fi
}

function f_load_key {
  # On récupère la liste des clefs SSH chargées
  local SSH_ADD_LIST=$($SSH_ADD_EXE -l)
	# Si aucune clef SSH n'est chargée dans l'agent SSH
	if [[ ! -n "$SSH_ADD_LIST" ]] && [[ -z "$SSH_ADD_LIST" ]]
	then
    # Pour chaques clefs présentes dans le workspace
    for key in ${SSH_AVAILABLE_KEYS};
    do
      # On vérifie le type du fichier
      local file_type_result=$($FILE_EXE "$key")
      f_check_return_code
      # Si le fichier est une clef privée
      if [[ "${file_type_result##*: }" == "$PEM_FILE_TYPE" ]]
      then
        printf "Chargement de la clef SSH <%s> ..." "${SSH_KEY##*/}"
        "$SSH_ADD_EXE -l $key"
        f_check_return_code
      fi
    done
	fi
}

function f_check_return_code {
	# Récupère le code de retour d'une commande
	local return_code=$?
	# Si le code retour est différent de 0, on stoppe l'exécution du script
	if [[ $return_code -ne "0" ]]
	then
		printf "Oops ! Erreur lors de l'exécution du script !"
		exit 1;
	fi
}

#
## Exécution principale du script
#

f_start_agent
