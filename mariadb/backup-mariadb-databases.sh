#!/bin/bash

#
## @file: backup-mariadb-databases.sh
## @author: Joel LE CORRE <git@sublimigeek.fr>
## @brief: script used to dump all existing MariaDB databases into a specific folder
## @links: https://gitlab.com/jlecorre/sysadmin-tools
#

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Script configuration
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Required MariaDB configuration keys
MARIADB_USERNAME=""
MARIADB_PASSWORD=""
MARIADB_HOST=""
MARIADB_EXECUTABLE_PATH="/usr/bin/mysql"
MARIADB_DUMP_EXECUTABLE_PATH="/usr/bin/mysqldump"

# Mail configuration
EMAIL_SRC=""
EMAIL_DST=""
EMAIL_OBJECT_PREFIX=""

# Backups configuration
MARIADB_BACKUP_FOLDER_DIR=""
MARIADB_BACKUP_NAME_PREFIX=""
MARIADB_BACKUP_NAME_SUFFIX=$(date '+%Y-%m-%d-%H-%M')
MARIADB_BACKUP_LOG_FOLDER="/var/log/mysql"
MARIADB_BACKUP_LOG_FILE="${MARIADB_BACKUP_LOG_FOLDER}/${MARIADB_BACKUP_NAME_PREFIX}-${MARIADB_BACKUP_NAME_SUFFIX}.log"

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Functions definition
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

check_status_config()
{
  if [ -z "${EMAIL_SRC}" ] || [ -z "${EMAIL_DST}" ] || [ -z "${EMAIL_OBJECT_PREFIX}" ] \
    || [ -z "${MARIADB_USERNAME}" ] || [ -z "${MARIADB_PASSWORD}" ] \
    || [ -z "${MARIADB_HOST}" ] || [ -z "${MARIADB_BACKUP_FOLDER_DIR}" ]; then
    print_message "error" "One or more required variables need to be declared!"
    cat "${MARIADB_BACKUP_LOG_FILE}"
    exit 1
  fi
}

check_status_code()
{
  local _status=$1
  local _error_message=$2

  if [ "${_status}" -ne 0 ]; then
    print_message "error" "${_error_message}"

    send_mail "${EMAIL_OBJECT_PREFIX} - MariaDB backup report for ${MARIADB_BACKUP_NAME_SUFFIX} => ERROR" "${MARIADB_BACKUP_LOG_FILE}"
    exit 1
  fi
}

is_installed()
{
  if ! command -v "${MARIADB_DUMP_EXECUTABLE_PATH}" >/dev/null 2>&1
  then
    check_status_code "1" "The <mariadb-client> package is not installed!"
  else
    print_message "info" "MySQL dump binary is located here: ${MARIADB_DUMP_EXECUTABLE_PATH}"
  fi
}

is_folder_exists()
{
  local _folder_name=$1

  if [ ! -d "${_folder_name}" ]; then
    mkdir -p "${_folder_name}"
    check_status_code $? "Could not properly create required folder: ${_folder_name}"
    print_message "info" "<${_folder_name}> folder has been created."
  fi
}

send_mail()
{
  local _mail_title="$1"
  local _mail_body="$2"

  mail -E -r "${EMAIL_SRC}" -s "${_mail_title}" "${EMAIL_DST}" < "${_mail_body}"
}

print_message()
{
  local _message_kind=$1
  local _message=$2

  printf "[$(date '+%Y/%m/%d:%H:%M:%S')] - ${_message_kind^^} - %s\\n" "${_message^}" >> "${MARIADB_BACKUP_LOG_FILE}"
}


delete_previous_backup_files()
{
  find "${MARIADB_BACKUP_FOLDER_DIR}" -name "${MARIADB_BACKUP_NAME_PREFIX}-*.sql.gz" -type f -delete
  check_status_code $? "Could not properly delete existing previous dump files!"
  print_message "info" "Previous backups have been properly deleted"
}

dump_mariadb_databases()
{
  print_message "info" "Starting databases dump process..."

	${MARIADB_EXECUTABLE_PATH} --host="${MARIADB_HOST}" --user="${MARIADB_USERNAME}" --password="${MARIADB_PASSWORD}" -e 'status' >/dev/null 2>&1
  check_status_code $? "Could not check if MariaDB server is alive or if the given authentication credentials are correct!"

  # MariaDB dump flags used:
  # -B, --batch        Don't use history file. Disable interactive behavior.
  # -s, --silent       Be more silent. Print results with a tab as separator, each row on new line.
  # -e, --execute=name Execute command and quit. (Disables --force and history file.)
  # shellcheck disable=SC2086
  _databases_list="$(${MARIADB_EXECUTABLE_PATH} --host=${MARIADB_HOST} --user=${MARIADB_USERNAME} --password=${MARIADB_PASSWORD} -Bse 'show databases;')"
  check_status_code $? "Could not properly generate the databases list!"

  for _database in ${_databases_list}
  do
    ${MARIADB_DUMP_EXECUTABLE_PATH} --host="${MARIADB_HOST}" --user="${MARIADB_USERNAME}" --password="${MARIADB_PASSWORD}" --single-transaction --skip-events "${_database}" \
    | gzip > "${MARIADB_BACKUP_FOLDER_DIR}/${MARIADB_BACKUP_NAME_PREFIX}-${_database}-${MARIADB_BACKUP_NAME_SUFFIX}.sql.gz"
    check_status_code $? "Could not dump the database: ${_database}"
    print_message "info" "Database properly dumped: ${_database}"
  done

  _dumped_databases_number=$(find "${MARIADB_BACKUP_FOLDER_DIR}" -name "${MARIADB_BACKUP_NAME_PREFIX}-*.sql.gz" -type f -exec ls {} \; | wc -l)
  print_message "info" "Number of databases dumped during the process: ${_dumped_databases_number}"
  print_message "info" "Databases export process completed"
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Main program
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

is_installed
check_status_config

is_folder_exists "${MARIADB_BACKUP_LOG_FOLDER}"
is_folder_exists "${MARIADB_BACKUP_FOLDER_DIR}"

delete_previous_backup_files
dump_mariadb_databases

send_mail "${EMAIL_OBJECT_PREFIX} - MariaDB backup report for ${MARIADB_BACKUP_NAME_SUFFIX} => SUCCESS" "${MARIADB_BACKUP_LOG_FILE}"
