Set Gnome Desktop Interface
========

Just a little shell script used to :
* Kill all running conky widgets
* Return to default Gnome Desktop Configuration (gsettings)
* Start all my conky widgets
* Apply some little customisation of my Gnome Desktop Configuration

And that's all =) 
Nothing magical is it not ?!
