# Useful tips and tricks everyday life

## Security audit

```
cd /your/path/to/download/lynis
git clone https://github.com/CISOfy/lynis.git
# Update Lynis release
./lynis update check|info
# Perform local security scan
./lynis audit system
# Perform a Dockerfile scan
./lynis audit dockerfile /path/to/your/Dockerfile
```

## debcheckroot scan

Trusted root file system verification for Debian related distros like Ubuntu, EasyPeasy and others.

```
mkdir debcheckroot
cd debcheckroot
# Download the software
# https://www.elstel.org/debcheckroot/index.html
wget https://www.elstel.org/debcheckroot/debcheckroot-v2.2
chmod +x debcheckroot-v2.2
# Execute a scan
./debcheckroot-v2.2 /
```

## Rkhunter scan

```
apt install rkhunter
rkhunter --versioncheck # check for latest release of binary
rkhunter --update # run fingerprints database updates
rkhunter --check --rwo --sk # run checks and report only warnings
rkhunter --propupdate # update hashes of known files
```

## Chrootkit

### Manual scan

```
apt install chkrootkit
chkrootkit
```

### Run chkrootkit once a day and report results by mail with a crontab

```
00 5 * * * /usr/sbin/chkrootkit 2>&1 | mail -s "[security] Chkrootkit scan - $(date '+\%d.\%m.\%Y - \%H:\%M')" username@domain.tld
```

## Clamscam scan

This command lime can take several time to be properly executed.

```
# Be verbose
# Skip printing OK files
# Recursive scan
# Always follow directory and file symlinkks
# Continue scanning even after a detection
# Detect possibly unwanted applications
# Detect structured data
# Alert on broken executable files
# Alert on broken graphic files
# Alert on encrypted archives and files
# Alert on files containing VBA macros

# Update the signatures database
freshclam
# scan the system
clamscan -v -o -r --follow-dir-symlinks=2 --follow-file-symlinks=2 -z --detect-pua --detect-structured --alert-broken --alert-broken-media --alert-encrypted --alert-macros /
```

## IO monitoring

### Sysstat

```
apt install sysstat
iostat -c -d 2 --human -m
```

### I/O top

```
apt install iotop
iotop -o -P -a
```

## CPU monitoring

### Sysstat

```
apt install sysstat

# Display five reports of statistics for all processors at two second intervals
mpstat -P ALL 2 5

# Display CPU, IO and memory usage every 5 seconds for each available block devices
sar 5 -u ALL -b -d --human -r ALL -P ALL -S
```

### NMON

```
apt install nmon
nmon
```
