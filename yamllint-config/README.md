# Overwrite yamllint configuration file

## Description
This configuration file is useful to overwrite the default configuration file used by the binary `yamllint`.  
If you don't know this software yet, there is a short description for you : `A linter for YAML files`  

OK, now you are ready to read the [Github yamllint documentation](https://github.com/adrienverge/yamllint) to understand why I have created this configuration file and how it works !  

For more informations, you can also read this documentation : https://yamllint.readthedocs.io/  

## How to use this configuration file
Nothing more easy than this :  
```shell
cd /to/your/workspace
git clone this-wonderful-git-repository
ln -s this-wonderful-git-repository/yamllint-config/.yamllint.cfg ${HOME}
# or if you prefer 
ln -s this-wonderful-git-repository/yamllint-config/.yamllint.cfg /path/to/your/working/directory
```

## How to use yamllint tool
Once again, nothing very complicated :  
```shell
yamllint /path/to/your/yaml/file
```

That's all ! 
So enjoy =)
