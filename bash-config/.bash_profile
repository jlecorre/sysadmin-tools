# shellcheck disable=SC2148
#
## @file .bashrc_profile
## @author Joel LE CORRE <git@sublimigeek.fr>
## @brief Configuration file used by the bash environment
## @needs Create a symbolic link from this git repository to your $HOME
#

# Shellcheck configuration
# shellcheck disable=SC1091,SC1090

# Constant variables
BINARY_HOMEDIR="$HOME/.local/bin"

# Enable completion for kubectl CLI and aliases
if command -v kubectl 1>/dev/null 2>&1
then
  _kubectl_completion_script="${BINARY_HOMEDIR}/kubectl/completion/kubectl.completion"
  if [ -e "$_kubectl_completion_script" ] ; then
    source "$_kubectl_completion_script"
  fi
  complete -F __start_kubectl k
fi

# set PATH so it includes user's private bin if it exists
if [ -d "${BINARY_HOMEDIR}" ] ; then
    PATH="${BINARY_HOMEDIR}:$PATH"
fi

# Enable kubens completion
_kubens_completion_script="${BINARY_HOMEDIR}/kubectx/completion/kubens.bash"
if [ -e "$_kubens_completion_script" ] ; then
    source "$_kubens_completion_script"
fi

# Enable kctx completion
_kctx_completion_script="${BINARY_HOMEDIR}/kubectx/completion/kubectx.bash"
if [ -e "$_kctx_completion_script" ] ; then
    source "$_kctx_completion_script"
fi

# Enable helm completion
_helm_completion_script="${BINARY_HOMEDIR}/helm/completion/helm.completion"
if [ -e "$_helm_completion_script" ] ; then
    source "$_helm_completion_script"
fi

# Enable clusterctl completion
_clusterctl_completion_script="${BINARY_HOMEDIR}/clusterctl/completion/clusterctl.completion"
if [ -e "$_clusterctl_completion_script" ] ; then
    source "$_clusterctl_completion_script"
fi

# Enable kustomize completion
_kustomize_completion_script="${BINARY_HOMEDIR}/kustomize/completion/kustomize.completion"
if [ -e "$_kustomize_completion_script" ] ; then
    source "$_kustomize_completion_script"
fi

# Enable vcluster completion
_vcluster_completion_script="${BINARY_HOMEDIR}/vcluster/completion/vcluster.completion"
if [ -e "$_vcluster_completion_script" ] ; then
    source "$_vcluster_completion_script"
fi

# Enable openstack completion
# Renew completion script: openstack complete --shell bash > "${BINARY_HOMEDIR}/openstack/openstack.completion"
_openstack_completion_script="${BINARY_HOMEDIR}/openstack/openstack.completion"
if [ -e "$_openstack_completion_script" ] ; then
    source "$_openstack_completion_script"
fi

# Enable cheat.sh completion
# shellcheck disable=SC2086,SC2207,SC2086,SC2034
_cht_complete()
{
    local cur prev opts
    _get_comp_words_by_ref -n : cur

    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"
    opts="$(curl -s cheat.sh/:list)"

    if [ ${COMP_CWORD} = 1 ]; then
    COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    __ltrim_colon_completions "$cur"
    fi
    return 0
}
complete -F _cht_complete cheat.sh

# Enable completion for jiractl
if command -v jiractl 1>/dev/null 2>&1
then
  _jiractl_completion_script="${BINARY_HOMEDIR}/jiractl/jiractl.completion"
  if [ -e "$_jiractl_completion_script" ] ; then
    source "$_jiractl_completion_script"
  fi
fi
complete -F __start_jiractl jiramks jirampr jiraraas jirapu

# Enable completion for argocd
if command -v argocd 1>/dev/null 2>&1
then
  _argocd_completion_script="${BINARY_HOMEDIR}/argocd/completion/argocd.completion"
  if [ -e "$_argocd_completion_script" ] ; then
    source "$_argocd_completion_script"
  fi
fi

# Enable completion for nodejs
if command -v node 1>/dev/null 2>&1
then
  _nodejs_completion_script="${BINARY_HOMEDIR}/nodejs/completion/nodejs.completion"
  if [ -e "$_nodejs_completion_script" ] ; then
    source "$_nodejs_completion_script"
  fi
fi

# Enable completion for TemporalIO CLI
if command -v temporal 1>/dev/null 2>&1
then
  _temporal_completion_script="${BINARY_HOMEDIR}/temporal/completion/temporal.completion"
  if [ -e "$_temporal_completion_script" ] ; then
    source "$_temporal_completion_script"
  fi
fi

# Enable completion for Kubebuilder CLI
if command -v kubebuilder 1>/dev/null 2>&1
then
  _kubebuilder_completion_script="${BINARY_HOMEDIR}/kubebuilder/completion/kubebuilder.completion"
  if [ -e "$_kubebuilder_completion_script" ] ; then
    source "$_kubebuilder_completion_script"
  fi
fi

# Enable completion for Ktunnel CLI
if command -v ktunnel 1>/dev/null 2>&1
then
  _ktunnel_completion_script="${BINARY_HOMEDIR}/ktunnel/completion/ktunnel.completion"
  if [ -e "$_ktunnel_completion_script" ] ; then
    source "$_ktunnel_completion_script"
  fi
fi

# Enable completion for CDSCTL CLI
if command -v cdsctl 1>/dev/null 2>&1
then
  _cdsctl_completion_script="${BINARY_HOMEDIR}/cdsctl/completion/cdsctl.completion"
  if [ -e "$_cdsctl_completion_script" ] ; then
    source "$_cdsctl_completion_script"
  fi
fi

# Enable completion for AWS CLI
if command -v aws 1>/dev/null 2>&1
then
  _aws_completion_script="${BINARY_HOMEDIR}/aws/aws_completer"
  if [ -e "$_aws_completion_script" ] ; then
    complete -C "${_aws_completion_script}" aws
  fi
fi

# Enable completion for Stern CLI
if command -v stern 1>/dev/null 2>&1
then
    source <(stern --completion=bash)
fi

# Enable completion for Cilium CLI
if command -v cilium 1>/dev/null 2>&1
then
    source <(cilium completion bash)
fi
