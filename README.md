# Set of tools for sysadmin

## Short description of this repository

Tools used to manage GNU/Linux servers and more fun stuff!!  
This repository is used to centralize all my configuration files, scripts and other stuffs useful daily to administrate a GNU/Linux server.  

## Inside the repository

You can find some stuff about:

* __Bash__ (full customisation of your .bashrc and status of git repository)
* __Vim__ (vim advanced configuration and plugins that I daily use)
* __Screen__ (full customisation of your `.screenrc`)
* __Tmux__ (full customisation of your `.tmux.conf` file)
* __i3wm__ (full customisation of your `.config/i3/config` file)
* __i3status__ (full customisation of your `.config/i3status/config` file)
* __Dynamic MOTD__ (aka Message Of The Day), a script developed by myself
* Custom my Ubuntu Gnome Desktop Interface (personal usage)
* Tool for __ssh-agent management__ (work still in progress, maybe one day I will finish it ...)(or not)
* __Git__ global __configuration__ files (https://git-scm.com/docs/git-config)
* __Ansible configuration__ file (https://www.ansible.com/)
* __Yamllint configuration__ file (https://github.com/adrienverge/yamllint)
* __Borg__ a shell script based on the usage of Borg backup tool (https://www.borgbackup.org/)
* __MariaDB__ a shell script based on the usage of mysql cli to dump existing databases

Enjoy and `Merge Requests` are welcome of course :)

