#
## @file .bashrc
## @author Joel LE CORRE <git@sublimigeek.fr>
## @brief Fichier de configuration de l'environnement bash
## @needs Créer un lien symbolique de ce fichier depuis le dépôt GIT vers la HOME de votre utilisateur
#

# Shellcheck configuration
# shellcheck disable=SC1091,SC1090,SC2148

# Colorisation des résultats de la commande LS
# Chargement des options si le système est un "Apple like"
if command -v sw_vers >/dev/null 2>&1
then
  export LS_OPTIONS='-G'
else
  # Chargement des options pour les systèmes "GNU/Linux like"
  export LS_OPTIONS='--color=auto'
fi

# Désactiver le <ctrl+s> (contrôle de flux) pour autoriser le reverse history "backward"
# => <ctrl+s> permet de revenir en arrière sur la recherche demarrée avec <ctrl+r>
# Si <shell interactive> est actif dans les flags Bash, on active le reverse history
[[ $- == *i* ]] && stty -ixon # permet de supprimer l'erreur <inappropriate-ioctl-for-device>

# Activation support 256 couleurs pour xterm (Unix like OS)
if [ -e /lib/terminfo/x/xterm-256color ]
then
  export TERM='xterm-256color'
# Activation support 256 couleurs pour xterm (Apple like OS)
elif [ -e /usr/share/terminfo/78/xterm-256color ]
then
  export TERM='xterm-256color'
else
  export TERM='xterm-color'
fi

# Evite la duplication des lignes et/ou les lignes commençant par un espace dans l'historique de bash
HISTCONTROL=ignoreboth # /!\ Ne fonctionne pas si HISTTIMEFORMAT est actif

# Définition de la taille de l'historique
HISTSIZE=
HISTFILESIZE=

# Active l'horodatage des commandes dans l'historique de bash
HISTTIMEFORMAT='%F %T - ' # %F => %Y - %m - %d + %T => ( %H : %M : %S )

# Permet l'ajout au fichier d'historique (ne pas écraser l'historique)
shopt -s histappend

# Vérifier la taille de la fenêtre après chaque commande et, si nécessaire,
# mettre à jour les valeurs des lignes et des colonnes
shopt -s checkwinsize

# Enable aliases usage in script
shopt -s expand_aliases

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# Activation de l'autocompletion bash
if [ -f /etc/bash_completion ]; then
  . /etc/bash_completion
fi

# Alias definitions
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# Définition de l'éditeur de texte utilisé par defaut
export EDITOR="vim"

# Activation du mode VI de bash
set -o vi

# Enable complete-alias function
# https://github.com/cykerway/complete-alias
# shellcheck disable=SC1090
# shellcheck source=~/.local/bin/complete-alias/complete_alias
. "$HOME/.local/bin/complete-alias/complete_alias"

# Enable complete-alias for specific aliases
# https://github.com/cykerway/complete-alias
complete -F _complete_alias kg
complete -F _complete_alias kd
complete -F _complete_alias ke
complete -F _complete_alias klg
complete -F _complete_alias k

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Allow shell exec into a pod or a container given as parameters
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

k8s-exec-into()
{
  local pod_name="$1"
  local container_name="$2"

  if [ -n "${container_name}" ]; then
    OPTS="-c ${container_name}"
    kubectl exec -ti "${pod_name}" "${OPTS}" -- bash || sh || ash
  else
    kubectl exec -ti "${pod_name}" -- bash || sh || ash
  fi
}
# Export k8s-exec-into function
export -f k8s-exec-into

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Prise en compte des scripts présents dans /etc/profile.d
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if [ -d /etc/profile.d ]
then
  for file in /etc/profile.d/*.sh
  do
    if [ -r "$file" ]
    then
      . "$file"
    fi
  done
  unset file
fi

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Fonction get-ip-info() : collecte des informations concernant une @IP donnée
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

get-ip-info()
{
  local target_ip="${1}"
  # récupération des données disponibles pour l'adresse IP donnée en paramètre
  # si aucune adresse n'est donnée en paramètre, l'@IP de la machine est utilisée par défaut
  ip_information=$(curl -s http://ipinfo.io/"${target_ip}")
  # si le retour de la requête curl est vide alors on affiche un message d'erreur
  if [[ -z "$ip_information" ]]
  then
    printf "%s\n" "There is a problem with the ipinfo.io API, maybe try again?!"
  # sinon on retourne les informations collectées
  else
    printf "%s\n" "$ip_information"
  fi
}
# Export de la fonction check_package() pour permettre son utilisation
export -f get-ip-info

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Fonction check-package() : liste les paquets mal désinstallés du système
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

check-package()
{
  local packages_list
  # récupération des paquets ayant le status 'rc'
  packages_list=$(dpkg -l | grep -e '^rc' | awk '{print $2}')
  # si la liste de paquets est vide, on affiche un message
  if [[ -z "$packages_list" ]]
  then
    printf "%s\n" "There is no package to uninstall from your system!"
  # sinon on retourne la liste de paquets
  else
    printf "%s\n" "$packages_list"
  fi
}

# Export de la fonction check_package() pour permettre son utilisation
export -f check-package

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Fonction bak() : sauvegarder les fichiers|répertoires avant modifications
# @details : format du renommage : nom_fichier_origin_2017-09-10_12-26-48
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

bak()
{
  # Vérification de la présence d'un paramètre lors de l'appel de la fonction
  if [[ -z "${1}" ]]
  then
    printf "%s\n%s\n\t%s\n" "La fonction bak() nécessite un argument !" "Exemple d'utilisation :" "$ bak mon_super_fichier"
  else
    if ! cp -r "${1}" "${1%/}_origin_$(date +%Y-%m-%d_%H-%M-%S)"
    then
      printf "%s\n" "Ooops ! La sauvegarde de <${1}> ne s'est pas déroulée correctement !"
    fi
  fi
}

# Export de la fonction bak pour permettre son utilisation
export -f bak

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Fonction meteo() : consulter la météo d'une ville passée en paramètre
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

meteo()
{
  # Vérification de la présence d'un paramètre lors de l'appel de la fonction
  if [[ -z "${1}" ]]
  then
    printf "%s\n%s\n\t%s\n" "La fonction meteo() nécessite un argument !" "Exemple d'utilisation :" "$ meteo nom_ville_ciblee"
  else
    clear
    # Interrogation de l'API qui renvoie les informations météorologiques de la ville cible
    if ! curl -H "Accept-Language: fr" http://wttr.in/"${1^}" # Modification de la casse
    then
      printf "%s\n" "Ooops ! La météo de <${1}> n'a pas pu être récupérée !"
    fi
  fi
}

# Export de la fonction meteo() pour permettre son utilisation
export -f meteo

##########################################################################
# Configuration du prompt bash pour Git (PS1 modifications)              #
# https://github.com/git/git/raw/master/contrib/completion/git-prompt.sh #
##########################################################################

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Pour fonctionner, le paquet GIT doit être installé sur le système
# Installation pour Debian like : sudo apt install git
# Installation pour RedHat like : sudo yum install git
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Configuration du GIT-PROMPT
# Pour plus d'informations, voir l'entête du script git-prompt.sh
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Affiche le statut des fichiers unstaged (*) / staged (+) de la branche courante
export GIT_PS1_SHOWDIRTYSTATE=1
# Affiche le statut des fichiers stashed ($) de la branche courante
export GIT_PS1_SHOWSTASHSTATE=1
# Affiche le statut des fichiers non versionnés (%)
export GIT_PS1_SHOWUNTRACKEDFILES=1
# Affiche les différences entre la HEAD et la branche courante
# "<" indique que l'on a des commits de retard
# ">" indique que l'on a des commits d'avance
# "<>" indique que l'on a des divergences
# "=" indique qu'il n'y a pas de différence
# Valeurs possibles : verbose/name/legacy/git/svn
export GIT_PS1_SHOWUPSTREAM="auto"

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Activation du GIT prompt et définition de la fonction
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Si le script git-prompt.sh est présent, on l'importe
if [[ -e /usr/lib/git-core/git-sh-prompt ]]
then
  # Chargement du script git-prompt.sh pour les systèmes "Debian like"
  . /usr/lib/git-core/git-sh-prompt
elif [[ -e /usr/share/git-core/contrib/completion/git-prompt.sh ]]
then
  # Chargement du script git-prompt.sh pour les systèmes "RedHat like"
  . /usr/share/git-core/contrib/completion/git-prompt.sh
elif [[ -e /Library/Developer/CommandLineTools/usr/share/git-core/git-prompt.sh ]]
then
  # Chargement du script git-prompt.sh pour les systèmes "Apple like"
  . /Library/Developer/CommandLineTools/usr/share/git-core/git-prompt.sh
  . /Library/Developer/CommandLineTools/usr/share/git-core/git-completion.bash
else
  printf "%s\n" "======================================================"
  printf "%s\n" "Oops ! Impossible d'activer le prompt pour GIT !"
  printf "%s\n" "Merci d'installer le paquet GIT sur votre machine :"
  printf "%s\n" "sudo (apt|yum|brew) install git"
  printf "%s\n" "======================================================"
  return 1
fi

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Fonction permettant de modifier le prompt de bash en modifiant le PS1
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

add_git_bash_prompt()
{
  # Récupération de la HEAD d'un dépôt GIT
  local get_head
  get_head=$(git symbolic-ref HEAD 2>/dev/null)
  # Si on est dans un dépôt git, on modifie le PS1
  # shellcheck disable=SC2086,SC2070
  if [ -n ${get_head} ]
  then
    # Adapter le prompt en fonction des permissions de l'utilisateur
    if [[ $(id -u) != 0 ]]
    then
      # Prompt personnalisé pour utilisateurs "normaux" + git PS1
      PS1="$(add_pyenv_ps1_prompt)\[\e[00;36m\][\T][\u@\H][\w][\$?]$(add_kube_ps1_prompt)\[\033[0;31m\]$(__git_ps1 '(%s)')\[\e[0m\]\[\e[00;36m\]\[\e[0m\]\n\[\e[00;31m\]➱\[\e[0m\] "
    else
      # Prompt personnalisé pour "ROOT" + git PS1
      PS1="$(add_pyenv_ps1_prompt)\[\e[00;31m\][\T][\u@\H][\w][\$?]$(add_kube_ps1_prompt)\[\e[00;31m\]\[\033[0;36m\]$(__git_ps1 '(%s)')\[\e[0m\]\[\e[0m\]\n\[\e[00;31m\]➱\[\e[0m\] "
    fi
  fi
}

# Appel de la fonction add_git_bash_prompt() dans la PROMPT_COMMAND
PROMPT_COMMAND=add_git_bash_prompt

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Configuration du plugin Kubernetes Krew
# Lien : https://github.com/kubernetes-sigs/krew
# Liste des plugins : https://github.com/kubernetes-sigs/krew-index/blob/master/plugins.md
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Affiche la version de Python exécutée et le virtualenv activé dans le prompt
# Lien : https://github.com/pyenv/pyenv
# curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

add_pyenv_ps1_prompt()
{
  # Définir cette variable à true ou false pour activer (ou non) le prompt pyenv-virtualenvs
  local pyenv_ps1_is_enabled=true
  local color_red="\033[0;31m"
  local color_off="\033[0m"

  if "${pyenv_ps1_is_enabled}"
  then

    # Affiche le répertoire principal où se trouvent les versions et shims de Python
    export PATH="$HOME/.pyenv/bin:$PATH"
    # shellcheck disable=SC2155
    export PATH="$(pyenv root)/shims:$PATH"

    # Active automatiquement un environnement Python virtualenv basé sur les versions pyenv
    export PYENV_VIRTUALENV_DISABLE_PROMPT=1
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"

    # Si un environnement pyenv est activé
    if [[ -n "${PYENV_VERSION}" ]]
    then
      # On affiche le nom du virtualenv actif et la version de Python dans le prompt bash
      PYENV_PYTHON_VERSION=$(pyenv virtualenv-prefix)
      echo -n "${color_red}(${PYENV_PYTHON_VERSION##*/}:${PYENV_VIRTUAL_ENV##*/})${color_off}"
    fi
  fi
}

# Exécution de la fonction permettant l'activation du prompt add_pyenv_ps1_prompt
add_pyenv_ps1_prompt

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Affiche le nom du context/namespace Kubernetes en cours d'utilisation
# Lien : https://github.com/jonmosco/kube-ps1
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

add_kube_ps1_prompt()
{
  # Définir cette variable à true ou false pour activer (ou non) le prompt kube_ps1
  local kube_ps1_is_enabled
  kube_ps1_is_enabled=true

  if "${kube_ps1_is_enabled}"
  then
    # Emplacement du script kube_ps1
    local kube_ps1_for_linux
    kube_ps1_for_linux="$HOME/.local/bin/kube-ps1/kube-ps1.sh"

    if [[ -e "${kube_ps1_for_linux}" ]]
    then
      . "${kube_ps1_for_linux}"
      kube_ps1_is_sourced="1"
    else
      printf "%s\n\n" "================================================================="
      printf "%s\n" "Oops ! Impossible de sourcer le script kube_ps1"
      printf "%s\n" "RTFM : https://github.com/jonmosco/kube-ps1/blob/master/README.md"
      printf "%s\n\n" "================================================================="
      return 1
    fi

    # Configuration des options du prompt kube_ps1
    # Structure du prompt : (<symbol>|<context>:<namespace>)
    if [[ "${kube_ps1_is_sourced}" -eq 1 ]]
    then
      # Activation du prompt pour toutes les sessions Bash
      kubeon -g
      # Choix prefix/suffix du prompt
      export KUBE_PS1_PREFIX='['
      export KUBE_PS1_SUFFIX=']'
      # Ne ps utiliser d'image pour l'icone k8s
      export KUBE_PS1_SYMBOL_USE_IMG='false'
      # Affichage du namespace
      export KUBE_PS1_NS_ENABLE=true

      # Activation du prompt
      printf "%s" "$(kube_ps1)"
    fi
  fi
}

# Exécution de la fonction permettant l'activation du prompt kube_ps1
add_kube_ps1_prompt

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Configuration du paquet 'hh' si il est installé sur le système
# @sources : https://github.com/dvorka/hstr
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Vérification de la présence du paquet 'hh'
is_installed=$(dpkg -s hh >>/dev/null 2>&1; printf "%d" "$?")
if [[ "$is_installed" -eq 0 ]]
then
  # Configuration de bash pour personnaliser hstr
  export HH_CONFIG=hicolor          # get more colors
  export PROMPT_COMMAND="history -a; history -n; ${PROMPT_COMMAND}" # mem/file sync
  # Paramètres commentés pour ne pas surcharger 'ma' configuration personnelle
  #shopt -s histappend              # append new history items to .bash_history
  #export HISTFILESIZE=10000        # increase history file size (default is 500)
  #export HISTSIZE=${HISTFILESIZE}  # increase history size (default is 500)
  #bind '"\C-r": "\C-ahh -- \C-j"'  # bind hh to Ctrl-r in vi editing-mode
  #export HISTCONTROL=ignorespace   # leading space hides commands from history
fi

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Fonction ssl-check() : permet de vérifier rapidement l'état d'un certificat SSL
# @details : fourni les informations suivantes : issuer | subject | expiration date
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ssl-check()
{
  # Définition des variables
  declare check_ssl_return
  declare -a check_ssl_certificate=("${1}" "${2}")
  # Vérification de la présence des paramètres requis
  if [[ -z "${1}" || -z "${2}" ]]
  then
    # Si il manque un paramètre, on affiche un message à l'utilisateur
    printf "%s\n" "La fonction ssl_check() nécessite plusieurs arguments pour s'exécuter !"
    printf "%s\n\t%s\n" "Exemple d'utilisation :" "$ ssl_check 'server_name' 'server_port'"
    return 1
  else
    # Sinon on vérifie le statut du certificat SSL
    check_ssl_return=$(echo | openssl s_client -servername "${check_ssl_certificate[0]}" \
      -connect "${check_ssl_certificate[0]}:${check_ssl_certificate[1]}" 2>/dev/null \
      | openssl x509 -noout -issuer -subject -dates 2>/dev/null)
    # Si le contrôle du certificat SSL échoue, on affiche un message à l'utilisateur
    # shellcheck disable=SC2181
    if [[ $? -ne 0 ]]
    then
      printf "%s\n%s\n" "Ooops ! La vérification du certificate SSL [[ ${check_ssl_certificate[0]}:${check_ssl_certificate[1]} ]] a lamentablement échouée ..." "Même joueur, joue encore ?! :P"
      return 1
    else
      # Sinon on affiche le résultat du check SSL
      printf "%s" "========================================================================="
      printf "\n%s\n" "${check_ssl_return}"
      printf "%s\n" "========================================================================="
    fi
  fi
}

# Export de la fonction ssl_check() pour permettre son utilisation
export -f ssl-check

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Fonction generate-scp-path() : permet d'afficher à l'écran les informations utiles
# au transfert de fichiers sur une machine via l'outil SCP (scp user@host:path .)
# @details : fourni les informations suivantes : utilisateur | adresse ip | répertoire courant
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

generate-scp-path()
{
  # Définition des variables
  declare scp_path_return
  # Génération des informations à afficher à l'utilisateur
  # Si le contrôle du certificat SSL échoue, on affiche un message à l'utilisateur
  if ! scp_path_return="${USER}"@"$(ip -o -4 -f inet addr show eth0 | awk -F ' ' '{ sub("/[[:digit:]]+", ""); print $4 }')":"$(pwd)"
  then
    printf "%s\n%s\n" "Ooops ! La génération des informations utiles au programme SCP a lamentablement échouée ..." "Même joueur, joue encore ?! :P"
    return 1
  else
    # Sinon on affiche le résultat
    printf "%s" "========================================================================="
    printf "\n%s\n" "Push mode : scp [-r] . ${scp_path_return}"
    printf "%s\n" "Pull mode : scp [-r] ${scp_path_return}/* ."
    printf "%s\n" "========================================================================="
  fi
}

# Export de la fonction generate_scp_path() pour permettre son utilisation
export -f generate-scp-path

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Fonction start-gpg-agent() : permet de démarrer un agent GPG ou de charger la configuration
# de l'agent GPG déjà exécuté afin de le partager au travers de plusieurs sessions
# @source : https://manpages.debian.org/stretch/gnupg-agent/gpg-agent.1.en.html
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

start-gpg-agent()
{
  set -x
  # Réutilisation du GPG agent qui est déjà exécuté
  if [ -n "$SSH_AUTH_SOCK" ] && [ -n "$GPG_AGENT_INFO" ]
  then
    export GPG_AGENT_INFO
    SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
    export SSH_AUTH_SOCK
  # Sinon, on stoppe les agents "zombies" et on en relance un tout propre
  else
    # Arrêt de l'agent existant
    if pgrep -x gpg-agent
    then
      gpgconf --kill gpg-agent
    fi
    # Démarrage d'un nouvel agent avec le support SSH
    eval "$(gpg-agent --daemon --enable-ssh-support)"
    unset SSH_AGENT_PID
    if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
      SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
      export SSH_AUTH_SOCK
    fi
  fi
  set +x
}

# Set automatically required environment variables
GPG_TTY=$(tty)
SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
export GPG_TTY
export SSH_AUTH_SOCK

# Export de la fonction start_gpg_agent() pour permettre son utilisation
export -f start-gpg-agent

# Activation de la prise en charge du programme direnv
eval "$(direnv hook bash)"

# De-base64-ify selectionned text in a console
debase64ify() {
	if ! command -v xclip 1>/dev/null 2>&1 || ! command -v xsel 1>/dev/null 2>&1
	then
		printf "Xclip or Xsel are not installed!\nRun: sudo apt install xclip xsel\n"
	else
		xclip -selection clipboard -out | base64 --decode
	fi
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Fonction encrypt-file(): allow to easily encrypt or decrypt a file by using openssl tool
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

encrypt-file()
{
  local encryption_mode_to_use=$1
  local input_file=$2
  local output_file=$3

  if [[ -z ${encryption_mode_to_use} || -z ${input_file} || -z ${output_file} ]]
  then
    printf "%s" "# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #"
    printf "\n%s\n" "One or more parameters are missing:"
    printf "\t%s\n" "Mode: encrypt|decrypt"
    printf "\t%s\n" "Input: file to encrypt"
    printf "\t%s\n" "Output: file to decrypt"
    printf "\t%s\n" "Password: requested on prompt"
    printf "%s\n" "Example: encrypt-file encrypt /tmp/decrypted.file /tmp/encrypted.file"
    printf "%s\n" "Example: encrypt-file decrypt /tmp/encrypted.file /tmp/decrypted.file"
    printf "%s\n" "# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #"
    return 1
  fi

  if [ "${encryption_mode_to_use}" = "encrypt" ]
  then
    local openssl_parameter="-e"
    local encryption_mode="${encryption_mode_to_use}"
  else
    local openssl_parameter="-d"
    local encryption_mode="${encryption_mode_to_use}"
  fi

  printf "File to %s: %s\n" "${encryption_mode}" "${input_file}"
  printf "File %sed: %s\n" "${encryption_mode}" "${output_file}"
	openssl enc "${openssl_parameter}" -aes-256-cbc -pbkdf2 -salt -in "${input_file}" -out "${output_file}"
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Prise en compte de configuration bash supplémentaires
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if [ -r "${HOME}/.bashrc_custom" ]
then
	. "${HOME}/.bashrc_custom"
fi
