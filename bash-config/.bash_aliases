# shellcheck disable=SC2148
#
## @file .bashrc_aliases
## @author Joel LE CORRE <git@sublimigeek.fr>
## @brief Aliases configuration files
## @needs Create a symbolic link of this file in your $HOME
#

alias l='ls -alh --color=auto $LS_OPTIONS'
alias lr='ls -ltr'
alias show-path='echo -e ${PATH//:/\\n}'
alias dist-upgrade='sudo apt update && sudo apt upgrade && sudo apt dist-upgrade'
alias dist-clean-packages='sudo apt autoremove && sudo apt autoclean'
alias pg='ps aux | grep'
alias ghist='history | grep -i'
alias k='cd ..'
alias kk='cd ../..; ls -al $LS_OPTIONS'
alias kl='cd ..; ls -al $LS_OPTIONS'
alias j='cd -'
alias jl='cd -; ls -al $LS_OPTIONS'
alias grep='grep --color=auto'
alias gpg='/usr/bin/gpg2'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias grpe='/bin/grep --color=auto'
alias less='less -R'
alias mem-usage='ps -eo pid,ppid,cmd,%mem,%cpu --sort=-%mem | head'
alias cpu-usage='ps -eo pid,ppid,cmd,%mem,%cpu --sort=-%cpu | head'
alias list-devices='sudo lsblk -o NAME,FSTYPE,SIZE,MOUNTPOINT,LABEL,TYPE'
alias yamllint='yamllint -c ${HOME}/.yamllint.cfg'
alias ip='ip --color'
alias ipb='ip --color --brief'
alias get-my-ip='curl -s http://myexternalip.com/raw && echo -e ""'
alias pbcopy='xclip -selection clipboard'
alias pbpaste='xclip -selection clipboard -o'
alias mount='mount | column -t'
alias header='curl -I'
alias wget='wget -c'
alias kill-firefox='kill -9 $(pidof firefox)'
alias reload='source ~/.bashrc && clear'
alias list-opened-ports='netstat -a | grep LISTEN'
alias tree='tree -C --dirsfirst'
alias today='date +"%A, %B %d, %Y"'

# Source: @nixcraft from Twitter
alias rsync-help='rsync --help | grep -Ew -- "-[rlptgoD]"'

# Source: @climagic from Twitter
# shellcheck disable=SC2154
alias print-rainbow='yes "$(seq 231 -1 16)" | while read i; do printf "\x1b[48;5;${i}m\n"; sleep .02; done'

# From the "help alias"
# A trailing space in VALUE causes the next word to be checked for alias substitution when the alias is expanded
alias watch='watch '

# Useful aliases to manage images and container with docker CLI
# shellcheck disable=SC2154
alias clean-containers="for container in \$(docker container ps -qa); do docker container rm \${container}; done"
# shellcheck disable=SC2154
alias clean-images="for image in \$(docker image ls -qa); do docker image rm \${image}; done"

# Alias VIM
alias v='/usr/bin/vim'
alias vi='/usr/bin/vim'

# Alias GIT
alias gd='git diff --color '
alias ga='git add '
alias gp='git pull'
alias gpu='git push'
alias gl='git log'
alias glg="git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset \
  %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative"
alias vlg="git log --graph --pretty=format:'%h - %d %s (%cr) <%an>' | vim -R -c 'set filetype=git nowrap' -"
alias gci='git commit '
alias gc='git checkout '
alias gcb='git checkout -b '
alias gb='git branch -alvv '
alias gs='git status '
alias gr='git remote -v'
alias got='git '
alias get='git '
alias gut='git '
alias gf='git fetch --all --prune'
alias gclean='git fetch --prune && git branch --merged | egrep -v "(^\*|master|dev)" | xargs git branch -d'

# Alias used by Kubernetes and its ecosystem
alias kctx='kubectx'
alias kns='kubens'
alias k='kubectl'
alias kgn='kubectl get nodes -o wide'
alias kgnw='kubectl get nodes -o wide --watch-only'
alias kgns='kubectl get namespaces -o wide'
alias kgp='kubectl get pods -o wide --sort-by=.status.startTime'
alias kgpa='kubectl get pods -A -o wide'
alias kgpw='kubectl get pods --watch-only -o wide'
alias kgs='kubectl get services -o wide'
alias kgsa='kubectl get services -A -o wide'
alias kgsw='kubectl get services --watch-only -o wide'
alias kgep='kubectl get endpoints -o wide'
alias kgepa='kubectl get endpoints -A -o wide'
alias kgepw='kubectl get endpoints --watch-only -o wide'
alias kgev='kubectl get events --sort-by='\''{.lastTimestamp}'\'
alias kgevw='kubectl get events --watch-only -o wide'
alias kgeva='kubectl get events -A --sort-by='\''{.lastTimestamp}'\'
alias k8s-shell-exec="kubectl run shell -i --tty --rm --restart=Never --image nicolaka/netshoot -- /bin/bash"
alias klg='kubectl logs -f '
alias kg='kubectl get'
alias kd='kubectl describe'
alias ke='kubectl edit'
