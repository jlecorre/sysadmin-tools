# Generate dynamic MOTD ("Message Of The Day")

## Informations about MOTD :
Usefull informations on Wikipedia : [Read Wikipedia definition of Unix MOTD](https://en.wikipedia.org/wiki/Motd_%28Unix%29)

## Informations about this script :
This script can generate dynamically "message of the day" on your system.

## Informations about what ?!
This script is able to get the following informations :
* Hostname of your server
* Status of system memory
* Status of swap system
* Get IPv4 and IPv6 of all networks interface, including Docker interfaces
* Uptime of your server
* Informations about your kernel, CPU and operating system
* Informations about the temperature of your hard drive
* Informations about the last connection
* Informations about actives SSH connections
* Informations about capacity of your rootfs
* Informations about the load average of your server

## How to use it ?!

* First, you will need to edit your SSH config file.

Enabled SSH MOTD on your system and disabled last log print in your SSH config file.  
To do this, edit with your favorite text editor this file :

```
/etc/ssh/sshd_config
````

And add or uncomment this lines :
```
PrintMotd yes
PrintLastLog no
```

* Next, restart your SSH service :
```
service ssh restart           # for Debian and ubuntu  
systemctl restart ssh.service # for distributions with systemd
```

* After that, you will need to create this directory :
```
mkdir -p /etc/update-motd.d/
```

* Create a symbolic link on this shell script :
```
ln -s /path/to/git/clone/dynamique-motd/dynamic_motd.sh /etc/update-motd.d/10-dynamic_motd.sh
```

* Now, you can get the dynamic_motd.sh script and create a cron task :
```
crontab -e
```

* And add this task to automatically execute this script :
```
# run dynamic_motd.sh every 30 minutes
30 * * * * export TERM=xterm-256color; export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"; /etc/update-motd.d/10-dynamic_motd.sh > /etc/motd
```

This cron task will generate every 30 minutes the MOTD file.  
This message will be displayed every time you will open an SSH session on your server.

Have fun =)
